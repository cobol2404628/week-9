       IDENTIFICATION DIVISION.
       PROGRAM-ID.  EDIT5.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SSN  PIC VP(5)999        VALUE .00000423.
       01 LSN  PIC 999P(5)V        VALUE 45600000.00.
       01 SB   PIC 999P(9)         VALUE ZEROS.
       01 SN   PIC 9V9(8)          VALUE 1.11111111.
       01 LN   PIC 9(8)V9          VALUE 11111111.
       01 PS   PIC 99.9(8).
       01 PL   PIC ZZ,ZZZ,ZZ9.99.
       01 PB   PIC ZZZ,ZZZ,ZZZ,ZZ9.
       PROCEDURE DIVISION.
       BEGIN.
           MOVE SSN TO PS 
           MOVE LSN TO LN 
           DISPLAY "Small scaled = " PS 
           DISPLAY "Large scaled = " PL 
           
           ADD SSN TO SN 
           ADD LSN TO LN
           MOVE SN TO PS
           MOVE LN TO PL
           DISPLAY "Small = " PS
           DISPLAY "Large = " PL

           MOVE 123456789012 TO SB
           MOVE SB TO PB 
           DISPLAY "Billions = " PB
           
         

           . 